# Generated by Django 5.0.4 on 2024-04-16 12:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='user_pwd',
            field=models.CharField(default='qwe12345', max_length=255, verbose_name='用户密码'),
        ),
    ]
