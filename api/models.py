from django.db import models

# Create your models here.
"""
1. 生成迁移文件

python manage.py makemigrations

2. 同步到数据库中

python manage.py migrate
"""


class User(models.Model):
    token = models.CharField(verbose_name="Token", max_length=255, unique=True)
    user_id = models.CharField(verbose_name="用户id", max_length=255)
    nickname = models.CharField(verbose_name="用户默认名", max_length=255)
    user_email = models.CharField(verbose_name="用户邮箱", max_length=255)
    create_Time = models.CharField(verbose_name="账号创建时间", max_length=255)
    user_name = models.CharField(verbose_name="用户名",max_length=255)
    user_pwd = models.CharField(verbose_name="用户密码",max_length=255, default='qwe12345')


