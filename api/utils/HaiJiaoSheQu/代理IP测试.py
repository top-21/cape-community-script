import requests

# 请求地址

targetUrl = "https://www.baidu.com"

# 代理服务器

proxyHost = "117.94.116.54"

proxyPort = "64256"

proxyMeta = "http://%(host)s:%(port)s" % {

    "host": proxyHost,

    "port": proxyPort,

}

# pip install -U requests[socks] socks5

# proxyMeta = "socks5://%(host)s:%(port)s" % {


# "host" : proxyHost,


# "port" : proxyPort,


# }


proxies = {

    "http": proxyMeta,

    "https": proxyMeta

}

resp = requests.get(targetUrl, proxies=proxies)

print(resp.status_code)


print(resp.text)