import base64
import json

import requests


class HaiJiaoSheQuSpider:
    def __init__(self):
        self.__Domain = "https://www.hj87d29.top"
        self.__headers = {
            'accept': 'application/json, text/plain, */*',
            'accept-language': 'zh-CN,zh;q=0.9',
            'access-control-allow-credentials': 'true',
            # 'cookie': '_ga=GA1.1.1751858735.1713072267; _ga_H4G4E5X3FL=GS1.1.1713072267.1.1.1713072508.0.0.0',
            'mver': '211112203214',
            'referer': 'https://www.hj87d29.top/home',
            'sec-ch-ua': '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
            'sec-ch-ua-mobile': '?1',
            'sec-ch-ua-platform': '"Android"',
            'sec-fetch-dest': 'empty',
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Mobile Safari/537.36',
            'x-user-id': '171308205701',
            'x-user-token': '14d282739b2f412e8178a92fed57988b',
        }

    # 海角加密破解函数
    @staticmethod
    def __HaiJiaoJieMa(JMFN=None) -> dict:
        base64_encoded_string = JMFN
        if base64_encoded_string is None:
            return {"data":None}
            pass
        for i in range(3):
            base64_encoded_string = base64.b64decode(base64_encoded_string)
        # 将解码后的字节对象转换为字符串
        # decoded_string = base64_encoded_string.decode("utf-8")
        return json.loads(base64_encoded_string)

    # 自动转换成数据
    def __DefaultInfo(self, JsonData):
        if JsonData is None:
            return []
        response = self.__HaiJiaoJieMa(JMFN=JsonData['data'])
        return response

    # 获取热帖新闻
    def GetHot(self, Page):
        SpiderUrl = self.__Domain + "/api/topic/hot/topics"
        params = {
            "page": Page
        }
        response = self.__DefaultInfo(requests.get(url=SpiderUrl, headers=self.__headers, params=params).json())
        return response

    # 获取帖子的消息
    def GetPostsInfo(self, TopicId):
        SpiderUrl = self.__Domain + f"/api/topic/{TopicId}"
        response = self.__DefaultInfo(requests.get(url=SpiderUrl, headers=self.__headers).json())
        return response

    # 获取帖子的评论
    def GetPostsCommentsInfo(self, topic_id, page):
        SpiderUrl = self.__Domain + f"/api/comment/reply_list"
        params = {
            "page": page,
            "sort": "asc",
            "topic_id": topic_id,
            "search_type": 0,
            "user_id": 0
        }
        response = self.__DefaultInfo(requests.get(url=SpiderUrl, headers=self.__headers, params=params).json())
        return response

    # 获取视频
    def GetPostsVideo(self, VideoID, resource_id):
        SpiderUrl = self.__Domain + "/api/attachment"
        jsdata = {
            "id": VideoID,
            "resource_id": resource_id,
            "line": "",
            "resource_type": "topic"
        }
        response = self.__HaiJiaoJieMa(requests.post(url=SpiderUrl, headers=self.__headers, json=jsdata).json()['data'])
        Result = {
            "VideoUrl": self.__Domain +  response['remoteUrl'],
            "coverUrl": response['coverUrl']
        }
        return Result

    # 返回搜索热门栏目
    def GetSearchHotInfo(self):
        SpiderUrl = self.__Domain + "/api/ranking/search_words"
        response = self.__DefaultInfo( requests.get(url=SpiderUrl, headers=self.__headers).json())
        return response

    # 搜索
    def GetSearchInfo(self, key, page):
        SpiderUrl = self.__Domain + "/api/topic/search"
        jsdata = {
            "key": key,
            "node_id": 0,
            "page": page
        }
        response = self.__DefaultInfo(requests.post(url=SpiderUrl,headers=self.__headers,json=jsdata).json())
        return response


if __name__ == '__main__':
    sp = HaiJiaoSheQuSpider()
    import hashlib

    # 定义用户名和密码
    username = "yyuser"
    password = "qwe12345"

    # 用户代理字符串（User Agent），在Python中可能需要通过其他方式获取
    # 例如，通过请求头中的'User-Agent'获取。这里用一个示例字符串代替。
    user_agent = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Mobile Safari/537.36"

    # 合并用户名、密码和用户代理
    input_string = username + password + user_agent

    # 使用 hashlib 进行 MD5 加密
    hash = hashlib.md5(input_string.encode()).hexdigest()

    # 打印 MD5 哈希值
    print("MD5 哈希值:", hash)



